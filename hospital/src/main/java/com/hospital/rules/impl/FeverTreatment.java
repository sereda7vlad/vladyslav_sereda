package com.hospital.rules.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;

import java.util.List;

public class FeverTreatment extends AbstractTreatment {

	@Override
	public void treat(final List<Patient> patients, final List<Medicine> prescribedMedicine) {
		if (prescribedMedicine.contains(Medicine.PARACETAMOL) || prescribedMedicine.contains(Medicine.ASPIRIN)) {
			updatePatientsHealthStatus(patients, HealthStatus.FEVER, HealthStatus.HEALTHY);
		}
	}

	@Override
	protected boolean isDangerousTreatment(final List<Medicine> prescribedMedicine) {
		return false;
	}
}
