package com.hospital.rules.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;

import java.util.List;

public class TuberculosisTreatment extends AbstractTreatment {

	@Override
	public void treat(final List<Patient> patients, final List<Medicine> prescribedMedicine) {
		if (isDangerousTreatment(prescribedMedicine)) {
			updatePatientsHealthStatus(patients, HealthStatus.HEALTHY, HealthStatus.FEVER);
		}
		if(prescribedMedicine.contains(Medicine.ANTIBIOTIC)){
			updatePatientsHealthStatus(patients, HealthStatus.TUBERCULOSIS, HealthStatus.HEALTHY);
		}
	}

	@Override
	protected boolean isDangerousTreatment(List<Medicine> prescribedMedicine){
		return prescribedMedicine.contains(Medicine.ANTIBIOTIC) && prescribedMedicine.contains(Medicine.INSULIN);
	}
}
