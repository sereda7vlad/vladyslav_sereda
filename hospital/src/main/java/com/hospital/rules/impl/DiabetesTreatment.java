package com.hospital.rules.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;

import java.util.List;

public class DiabetesTreatment extends AbstractTreatment {

	@Override
	public void treat(final List<Patient> patients, final List<Medicine> prescribedMedicine) {
		if (isDangerousTreatment(prescribedMedicine)) {
			updatePatientsHealthStatus(patients, HealthStatus.DIABETES, HealthStatus.DEAD);
		}
	}

	@Override
	protected boolean isDangerousTreatment(final List<Medicine> prescribedMedicine) {
		return !prescribedMedicine.contains(Medicine.INSULIN);
	}
}
