package com.hospital.rules.impl;

import com.hospital.rules.Treatment;
import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;

import java.util.List;

public abstract class AbstractTreatment implements Treatment {

	protected void updatePatientsHealthStatus(List<Patient> patients, HealthStatus currentStatus, HealthStatus newStatus) {
		patients.stream()
				.filter(patient -> patient.getHealthStatus().equals(currentStatus))
				.forEach(patient -> patient.setHealthStatus(newStatus));
	}

	protected abstract boolean isDangerousTreatment(List<Medicine> prescribedMedicine);
}
