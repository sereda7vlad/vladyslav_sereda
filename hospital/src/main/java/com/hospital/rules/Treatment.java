package com.hospital.rules;

import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;

import java.util.List;

public interface Treatment {

	void treat(List<Patient> patients, List<Medicine> prescribedMedicine);

}
