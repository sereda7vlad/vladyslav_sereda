package com.hospital.entity;

public enum Medicine {

	ASPIRIN,
	PARACETAMOL,
	ANTIBIOTIC,
	INSULIN
}
