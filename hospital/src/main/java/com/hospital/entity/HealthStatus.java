package com.hospital.entity;

public enum HealthStatus {

	FEVER("F"),
	HEALTHY("H"),
	DIABETES("D"),
	TUBERCULOSIS("T"),
	DEAD("X");

	private String statusCode;

	HealthStatus(final String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusCode() {
		return statusCode;
	}
}
