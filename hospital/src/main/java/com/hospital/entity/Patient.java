package com.hospital.entity;

public class Patient {

	private HealthStatus healthStatus;

	public Patient(final HealthStatus healthStatus) {
		this.healthStatus = healthStatus;
	}

	public HealthStatus getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(final HealthStatus healthStatus) {
		this.healthStatus = healthStatus;
	}
}
