package com.hospital.report.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Patient;
import com.hospital.report.PatientsStatisticReportGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PatientsStatisticReportGeneratorImpl implements PatientsStatisticReportGenerator {

	private static final String HEALTH_STATUS_PATIENT_SEPARATOR = " ";
	private static final String HEALTH_STATUS_AMOUNT_SEPARATOR = ":";
	private static final long DEFAULT_ZERO_VALUE = 0L;

	@Override
	public String generateReport(List<Patient> patients) {
		Map<HealthStatus, Long> rawReportData = generateRawReportData(patients);
		return joiningRawReportDataByHealthStatus(rawReportData);
	}

	private Map<HealthStatus, Long> generateRawReportData(List<Patient> patients) {
		return patients.stream()
				.map(Patient::getHealthStatus)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}

	private String joiningRawReportDataByHealthStatus(Map<HealthStatus, Long> rawReportData) {
		return Arrays.stream(HealthStatus.values())
				.map(code -> createPatientsGroupDataRecord(code, rawReportData))
				.collect(Collectors.joining(HEALTH_STATUS_PATIENT_SEPARATOR));
	}

	private String createPatientsGroupDataRecord(HealthStatus healthStatus, Map<HealthStatus, Long> rawReportData) {
		return healthStatus.getStatusCode() + HEALTH_STATUS_AMOUNT_SEPARATOR + rawReportData.getOrDefault(healthStatus, DEFAULT_ZERO_VALUE);
	}
}
