package com.hospital.report;

import com.hospital.entity.Patient;

import java.util.List;

public interface PatientsStatisticReportGenerator {

	String generateReport(List<Patient> inputData);
}
