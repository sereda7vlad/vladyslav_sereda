package com.hospital.service.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;
import com.hospital.rules.Treatment;
import com.hospital.rules.impl.DiabetesTreatment;
import com.hospital.rules.impl.FeverTreatment;
import com.hospital.rules.impl.TuberculosisTreatment;
import com.hospital.service.Doctor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DoctorImpl implements Doctor {

	private static final List<Medicine> LETHAL_COMBINATION_OF_MEDICINE = Arrays.asList(Medicine.ASPIRIN, Medicine.PARACETAMOL);

	private List<Patient> patients;
	private List<Medicine> prescribedMedicine;
	private List<Treatment> treatments;


	public DoctorImpl() {
		this.patients = new ArrayList<>();
		prescribedMedicine = new ArrayList<>();
		treatments = new ArrayList<>();
		treatments.add(new TuberculosisTreatment());
		treatments.add(new FeverTreatment());
		treatments.add(new DiabetesTreatment());
	}

	@Override
	public void prescribesMedicine(final Medicine medicine) {
		prescribedMedicine.add(medicine);
		if (prescribedMedicine.containsAll(LETHAL_COMBINATION_OF_MEDICINE)) {
			patients.forEach(patient -> patient.setHealthStatus(HealthStatus.DEAD));
		}
	}

	@Override
	public void acceptTreatment() {
		treatments.forEach(treatment -> treatment.treat(patients, prescribedMedicine));
	}

	@Override
	public void addPatients(final List<Patient> patients) {
		this.patients.addAll(patients);
	}

	@Override
	public List<Patient> getPatients() {
		return this.patients;
	}
}
