package com.hospital.service;

import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;

import java.util.List;

public interface Doctor {

	void prescribesMedicine(Medicine medicine);

	void acceptTreatment();

	void addPatients(List<Patient> patients);

	List<Patient> getPatients();
}
