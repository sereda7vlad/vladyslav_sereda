package com.hospital;

import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;
import com.hospital.parser.PatientsInputParser;
import com.hospital.parser.impl.PatientsInputParserImpl;
import com.hospital.report.PatientsStatisticReportGenerator;
import com.hospital.report.impl.PatientsStatisticReportGeneratorImpl;
import com.hospital.service.Doctor;
import com.hospital.service.impl.DoctorImpl;

import java.util.List;

public class Quarantine {

	private static final String PATIENTS_INPUT_SEPARATOR = ",";

	private PatientsStatisticReportGenerator patientsStatisticReportGenerator = new PatientsStatisticReportGeneratorImpl();
	private PatientsInputParser stringPatientsParser = new PatientsInputParserImpl(PATIENTS_INPUT_SEPARATOR);

	private Doctor doctor;

	public Quarantine(String subjects) {
		List<Patient> patients = stringPatientsParser.parse(subjects);
		doctor = new DoctorImpl();
		doctor.addPatients(patients);
	}

	public void aspirin() {
		doctor.prescribesMedicine(Medicine.ASPIRIN);
	}

	public void antibiotic() {
		doctor.prescribesMedicine(Medicine.ANTIBIOTIC);
	}

	public void insulin() {
		doctor.prescribesMedicine(Medicine.INSULIN);
	}

	public void paracetamol() {
		doctor.prescribesMedicine(Medicine.PARACETAMOL);
	}

	public void wait40Days() {
		doctor.acceptTreatment();
	}

	public String report() {
		return patientsStatisticReportGenerator.generateReport(doctor.getPatients());
	}
}
