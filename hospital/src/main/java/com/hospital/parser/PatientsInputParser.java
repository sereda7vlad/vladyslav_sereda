package com.hospital.parser;

import com.hospital.entity.Patient;

import java.util.List;

public interface PatientsInputParser {

	List<Patient> parse(String input);
}
