package com.hospital.parser.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Patient;
import com.hospital.parser.PatientsInputParser;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PatientsInputParserImpl implements PatientsInputParser {

	private String inputSeparator;
	private Map<String, HealthStatus> healthStatusMapping;

	private PatientsInputParserImpl() {
	}

	public PatientsInputParserImpl(String inputSeparator) {
		this.inputSeparator = inputSeparator;
		initHeathStatusMapping();
	}

	@Override
	public List<Patient> parse(String inputData) {

		List<String> codes = Arrays.asList(inputData.split(inputSeparator));
		return codes.stream()
				.map(healthStatusMapping::get)
				.filter(Objects::nonNull)
				.map(Patient::new)
				.collect(Collectors.toList());
	}

	private void initHeathStatusMapping() {
		this.healthStatusMapping = Arrays.stream(HealthStatus.values())
				.collect(Collectors.toMap(HealthStatus::getStatusCode, Function.identity()));
	}

}
