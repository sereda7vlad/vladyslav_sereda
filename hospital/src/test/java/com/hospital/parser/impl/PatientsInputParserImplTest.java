package com.hospital.parser.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Patient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PatientsInputParserImplTest {

	private static final String PATIENTS_INPUT_SEPARATOR = ",";

	@InjectMocks
	private PatientsInputParserImpl parser = new PatientsInputParserImpl(PATIENTS_INPUT_SEPARATOR);

	@Before
	public void setUp() {
	}

	@Test
	public void shouldCreatePatientsListWithHealthStatusByCharacters() {
		//when
		List<Patient> actual = parser.parse("F,D,D,H");
		//then
		assertThat(actual.size()).isEqualTo(4);
		assertThat(actual.get(0)).hasFieldOrPropertyWithValue("healthStatus", HealthStatus.FEVER);
		assertThat(actual.get(1)).hasFieldOrPropertyWithValue("healthStatus", HealthStatus.DIABETES);
		assertThat(actual.get(2)).hasFieldOrPropertyWithValue("healthStatus", HealthStatus.DIABETES);
		assertThat(actual.get(3)).hasFieldOrPropertyWithValue("healthStatus", HealthStatus.HEALTHY);
	}

	@Test
	public void shouldNotCreatePatientsWhenCharactersSeparatedByWrongDelimiter() {
		//when
		List<Patient> actual = parser.parse("D:F");
		//then
		assertThat(actual).isEmpty();
	}


}