package com.hospital.rules.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DiabetesTreatmentTest {

	@InjectMocks
	private DiabetesTreatment treatment;


	private Patient firstPatient;
	private Patient secondPatient;
	private List<Patient> patients;
	private List<Medicine> medicines;

	@Before
	public void setUp() {
		firstPatient = new Patient(HealthStatus.DIABETES);
		secondPatient = new Patient(HealthStatus.TUBERCULOSIS);
		patients = Arrays.asList(firstPatient, secondPatient);
		medicines = Arrays.asList(Medicine.ANTIBIOTIC, Medicine.INSULIN);
	}

	@Test
	public void shouldTreatDiabetesCorrectly() {
		//when
		treatment.treat(patients, medicines);
		//then
		assertThat(firstPatient.getHealthStatus()).isEqualTo(HealthStatus.DIABETES);
		assertThat(secondPatient.getHealthStatus()).isEqualTo(HealthStatus.TUBERCULOSIS);
	}

	@Test
	public void shouldTreatDiabetesWithoutInsulinAndLeadsToDeath() {
		//given
		medicines = Arrays.asList(Medicine.ANTIBIOTIC, Medicine.PARACETAMOL);
		//when
		treatment.treat(patients, medicines);
		//then
		assertThat(firstPatient.getHealthStatus()).isEqualTo(HealthStatus.DEAD);
		assertThat(secondPatient.getHealthStatus()).isEqualTo(HealthStatus.TUBERCULOSIS);
	}

}