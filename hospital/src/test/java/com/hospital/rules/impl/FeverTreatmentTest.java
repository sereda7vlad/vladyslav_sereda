package com.hospital.rules.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class FeverTreatmentTest {

	@InjectMocks
	private FeverTreatment treatment;

	private Patient firstPatient;
	private Patient secondPatient;
	private List<Patient> patients;
	private List<Medicine> medicines;

	@Before
	public void setUp() {
		firstPatient = new Patient(HealthStatus.DIABETES);
		secondPatient = new Patient(HealthStatus.FEVER);
		patients = Arrays.asList(firstPatient, secondPatient);
		medicines = Arrays.asList(Medicine.ANTIBIOTIC, Medicine.PARACETAMOL);
	}

	@Test
	public void shouldTreatFeverCorrectlyWithParacetamol() {
		//when
		treatment.treat(patients, medicines);
		//then
		assertThat(firstPatient.getHealthStatus()).isEqualTo(HealthStatus.DIABETES);
		assertThat(secondPatient.getHealthStatus()).isEqualTo(HealthStatus.HEALTHY);
	}

	@Test
	public void shouldTreatFeverCorrectlyWithAspirin() {
		//given
		medicines = Arrays.asList(Medicine.ANTIBIOTIC, Medicine.ASPIRIN);
		//when
		treatment.treat(patients, medicines);
		//then
		assertThat(firstPatient.getHealthStatus()).isEqualTo(HealthStatus.DIABETES);
		assertThat(secondPatient.getHealthStatus()).isEqualTo(HealthStatus.HEALTHY);
	}

	@Test
	public void shouldDontTreatFeverWithoutMedicine() {
		//given
		medicines = Arrays.asList(Medicine.ANTIBIOTIC, Medicine.INSULIN);
		//when
		treatment.treat(patients, medicines);
		//then
		assertThat(firstPatient.getHealthStatus()).isEqualTo(HealthStatus.DIABETES);
		assertThat(secondPatient.getHealthStatus()).isEqualTo(HealthStatus.FEVER);
	}
}