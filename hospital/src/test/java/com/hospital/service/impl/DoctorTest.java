package com.hospital.service.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Medicine;
import com.hospital.entity.Patient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DoctorTest {

	@InjectMocks
	private DoctorImpl doctor;

	private Patient firstPatient;
	private Patient secondPatient;

	@Before
	public void setUp() {
		firstPatient = new Patient(HealthStatus.DIABETES);
		secondPatient = new Patient(HealthStatus.TUBERCULOSIS);

		doctor.addPatients(Arrays.asList(firstPatient, secondPatient));
	}

	@Test
	public void shouldAddMedicineWithoutPatientStatusChange() {
		//when
		doctor.prescribesMedicine(Medicine.PARACETAMOL);
		doctor.prescribesMedicine(Medicine.INSULIN);
		//then
		assertThat(firstPatient.getHealthStatus()).isEqualTo(HealthStatus.DIABETES);
		assertThat(secondPatient.getHealthStatus()).isEqualTo(HealthStatus.TUBERCULOSIS);
	}

	@Test
	public void shouldAddLethalMedicineCombinationWithDeadPatientStatusChange() {
		//when
		doctor.prescribesMedicine(Medicine.PARACETAMOL);
		doctor.prescribesMedicine(Medicine.ASPIRIN);
		//then
		assertThat(firstPatient.getHealthStatus()).isEqualTo(HealthStatus.DEAD);
		assertThat(secondPatient.getHealthStatus()).isEqualTo(HealthStatus.DEAD);
	}

}