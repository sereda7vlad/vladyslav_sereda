package com.hospital.report.impl;

import com.hospital.entity.HealthStatus;
import com.hospital.entity.Patient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PatientsStatisticReportGeneratorImplTest {

	@InjectMocks
	private PatientsStatisticReportGeneratorImpl reporter;

	@Before
	public void setUp(){
	}

	@Test
	public void shouldGenerateEmptyReportWhenNoPatients() {
		//given
		String expected = "F:0 H:0 D:0 T:0 X:0";
		//when
		String actual = reporter.generateReport(Collections.emptyList());
		//then
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void shouldGenerateReportWithDifferentPatients() {
		//given
		List<Patient> patientList = Arrays.asList(
				new Patient(HealthStatus.FEVER),
				new Patient(HealthStatus.HEALTHY),
				new Patient(HealthStatus.HEALTHY),
				new Patient(HealthStatus.TUBERCULOSIS)
		);
		String expected = "F:1 H:2 D:0 T:1 X:0";
		//when
		String actual = reporter.generateReport(patientList);
		//then
		assertThat(actual).isEqualTo(expected);
	}




}